// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the DLLINJ_HOOK_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// DLLINJ_HOOK_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#pragma once

#ifdef DLLINJ_HOOK_EXPORTS
#define DLLINJ_HOOK_API __declspec(dllexport)
#else
#define DLLINJ_HOOK_API __declspec(dllimport)
#endif

extern HINSTANCE g_hinstDll;

DLLINJ_HOOK_API bool DIH_GetProcessId(LPCTSTR pname, DWORD* pid);

DLLINJ_HOOK_API void DIH_SetWindowHandleForLoggedText(HWND hWndLabel);

DLLINJ_HOOK_API bool DIH_SetKeyboardHookOnProcess(DWORD dwProcessId);

DLLINJ_HOOK_API bool DIH_SetKeyboardHookOnThread(DWORD dwThreadId);

DLLINJ_HOOK_API void DIH_UnhookAll();
